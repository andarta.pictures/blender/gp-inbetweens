# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

bl_info = {
    "name" : "GP_INBETWEENS",
    "author" : "Tom VIGUIER",
    "description" : "Create 2D animation inbetweens, faster",
    "blender" : (3, 0, 1),
    "version" : (0, 0, 1),
    "location" : "hit F8 over dopesheet or 3D viewport",
    "warning" : "EXPERIMENTAL TOOL",
    "category" : "Andarta"
}

import bpy
import rna_keymap_ui
import blf
#import time


def copy_strokes(src_frame, tgt_frame):
    tgt_frame.clear()
    for src_stroke in src_frame.strokes:
        tgt_stroke = tgt_frame.strokes.new()
        for attr in dir(src_stroke):
            try:
                setattr(tgt_stroke, attr, getattr(src_stroke, attr))
            except : pass
        tgt_stroke.points.add(len(src_stroke.points))
        for i, src_point in enumerate(src_stroke.points):
            for attr in dir(src_point):
                try :
                    setattr(tgt_stroke.points[i], attr, getattr(src_point, attr))
                except : pass
        tgt_stroke.points.update()


def get_objects_and_childs(objects, objects_list):
    result = []
    for object in objects :
        if object not in result :
            result.append(object)
        for child in get_constraint_children_recursive(object, objects_list, []) :
            if child not in result :
                result.append(child)
    return result

def get_constraint_children_recursive(object, objects_list, ctrl_list):
    '''
    returns a list of all the objects in the dependancy tree (childrens, 'child of' constraints targeting the object) recursively
    args : object = the object you wan't to get childrens from
           objects_list = the list where you want to look for children (basically bpy.context.scene)
           ctrl_list = provide an empty list, this is to avoid infinite recursions
    '''
    childs = [] 
    for child in object.children_recursive :
        if child not in childs :
            childs.append(child)  
            
    for ob in objects_list :
        if ob not in childs and len([constraint for constraint in ob.constraints if constraint.type == 'CHILD_OF' and constraint.target == object]) > 0 :
            childs.append(ob)
                
    for child in childs :
        if child not in ctrl_list :
            ctrl_list.append(child)
            for granchild in get_constraint_children_recursive(child, objects_list, ctrl_list) :
                if granchild not in childs :
                    childs.append(granchild)
    return childs

def move_gp_frame(layer, frame, frame_number):
    '''move a gp frame along the timeline
    the main issue of this method it that every frame of the layer are copies, and direct references to them will now lead to an error
    args : layer = layer whare to move the keyframe
           frame = frame to move
           frame_number = where to move the frame
    returns : the moved frame (note that references to other frames will be corrupted)
    '''
    temp_layer = frame.id_data.layers.new('temp')
    for src_frame in layer.frames : 
        temp_fr = temp_layer.frames.copy(src_frame)
        if src_frame == frame :
            temp_fr.frame_number = frame_number      
    d = dict()  
  
    for i, temp_fr in enumerate(temp_layer.frames) :
        d[temp_fr.frame_number] = i
        

    layer.clear()  

    for key in sorted(d) :
        i = d[key]
        new_fr = layer.frames.copy(temp_layer.frames[i])
        if new_fr.frame_number == frame_number :
            moved_frame = new_fr  

    for j, frame in enumerate(layer.frames) :   
        if frame == moved_frame :
            before_frame = layer.frames[j-1]        
            after_frame = layer.frames[j+1]
            break
        
    layer.id_data.layers.remove(temp_layer) 
    #still need to kind of refresh by frame change
    bpy.context.scene.frame_set(bpy.context.scene.frame_current +1) 
    bpy.context.scene.frame_set(bpy.context.scene.frame_current -1) 
    return moved_frame, before_frame, after_frame

def update_func(self, context): # verifier que self existe via is instance
    if self.factor > 150:
        self.factor = 150
    if self.factor < -50:
        self.factor = -50

def sculpted_frames(frame_1, frame_2):
    if len(frame_1.strokes) > 0 and len(frame_2.strokes) > 0 and len(frame_1.strokes) == len(frame_2.strokes):
        for i, stroke_1 in enumerate(frame_1.strokes) :
            stroke_2 = frame_2.strokes[i]
            if len(stroke_1.points) != len(stroke_2.points) :
                return False #not the same amount of points in a stroke    
        for i, stroke_1 in enumerate(frame_1.strokes) :
            stroke_2 = frame_2.strokes[i]    
            for j, point_1 in enumerate(stroke_1.points) :
                point_2 = stroke_2.points[j]
                if point_1.co != point_2.co :
                    return True
        return False #not sculpted but exactly the same
    else :
        return False #not the same amount of strokes



def get_before_after_gpf(current_frame, layer):
    gpf_before = None
    gpf_after = None
    for frame in layer.frames :
        if frame.frame_number < current_frame :
            gpf_before = frame
        if frame.frame_number == current_frame :
            return None, None
        if frame.frame_number > current_frame :
            gpf_after = frame
            break
    if gpf_before and gpf_after :
        return gpf_before, gpf_after
    else : 
        return None, None

def get_before_after_kf(current_frame, fcurve) :
    kf_before = None
    kf_after = None
    for keyframe in fcurve.keyframe_points :
        if keyframe.co.x < current_frame :
            kf_before = keyframe
        if keyframe.co.x == current_frame :
            return None, None
        if keyframe.co.x > current_frame :
            kf_after = keyframe
            break
    if kf_before and kf_after :
        return kf_before, kf_after
    else :
        return None, None

def get_now_kf(current_frame, fcurve):
    for keyframe in fcurve.keyframe_points : 
        if keyframe.co.x == current_frame :
            return keyframe
            break
    return None

def get_obj_children(obj, list) :
    if obj not in list :
        list.append(obj)
        #print(obj)
    for child in obj.children :
        if child not in list :
            list.append(child)
            #print(child)
        for granchild in child.children :
            get_obj_children(child, list)
    return list

def fc_is_from_array(fcurve) :
    if len([fc for fc in fcurve.id_data.fcurves if fc.data_path == fcurve.data_path]) > 1 :
        return True
    else :
        return False

class GPTWEEN_OT_MODAL(bpy.types.Operator):
    
    bl_idname = "gptween.modal"
    bl_label = "generate smart 2D inbetweens"
    bl_options = {'UNDO'}

    first_mouse_x: bpy.props.IntProperty(options={'HIDDEN'})
    first_value: bpy.props.FloatProperty(options={'HIDDEN'})
    factor : bpy.props.IntProperty(default = 50, update=update_func)
    above_half : bpy.props.BoolProperty(default = False, options={'HIDDEN'})
    
    clip_array_index : bpy.props.IntProperty(default = -1, options={'HIDDEN'})
    previous_event_type : bpy.props.StringProperty(options={'HIDDEN'})

    fcurves_to_interpolate = []
    number_objects: bpy.props.IntProperty(options={'HIDDEN'})

    layers_to_use = []
    random_event = []
    font_id = 0
    handler = []
    num_input_bool : bpy.props.BoolProperty(default = False, options={'HIDDEN'})

    def abort(self, context):
        preferences = context.preferences
        addon_prefs = preferences.addons[__name__].preferences 
        for i in self.fcurves_to_interpolate :
            fcurve = i[0]
            kf_new = i[2]
            fcurve.keyframe_points.remove(kf_new)
            fcurve.keyframe_points.update()    
        
        if addon_prefs.create_gp_frame :
            for i in self.layers_to_use :
                layer = i[0]
                gpf = i[2]
                if gpf :
                    layer.frames.remove(gpf)  
                    layer.frames.update()           
        return

    def interpolate(self, context) :
        #print('interpolate')
        preferences = context.preferences
        addon_prefs = preferences.addons[__name__].preferences 
        current_frame = context.scene.frame_current 
        
        #clipping when factor crosses 50%
        clip_switch = None
        if self.factor >= 50 and self.above_half == False :
            self.above_half = True
            clip_switch = 'AFTER'         
        elif self.factor < 50 and self.above_half == True :
            self.above_half = False
            clip_switch = 'BEFORE'
        #interpolate fcurves
        for i in self.fcurves_to_interpolate :
            fcurve = i[0]
            before_value = i[1]
            kf_new = i[2]
            after_value = i[3]
            
            '''if fcurve.data_path == '["FLIP"]'  :
                kf_new.co.y = round(self.factor/100,0) #min([before_value , after_value])
                #print(round(self.factor/100, 1))'''

            if self.clip_array_index != -1 and fcurve.data_path == 'location' and fcurve.array_index == self.clip_array_index :
                if clip_switch == 'BEFORE':
                    kf_new.co.y = before_value
                if clip_switch == 'AFTER' :
                    kf_new.co.y = after_value
            
            else:
                tween_value = ((after_value - before_value) * (self.factor/100)) + before_value
                kf_new.co.y = tween_value
        #gpf_time = time.time()
        #print('fcurves:', start_time - time.time())
        #gp frame clipping
        if addon_prefs.create_gp_frame :
            #duplicate list
            layers = []
            for i in self.layers_to_use: 
                layers.append(i)

            #interpolate or replace gp frame
            for i in layers :
                layer = i[0]
                gpf_before = i[1]
                new_gpf = i[2]
                gpf_after = i[3]
                interpolate = i[4]
                
                if interpolate == True :
                    for j, stroke in enumerate(new_gpf.strokes ):
                        for k, point in enumerate(stroke.points) :
                            for l , co in enumerate(point.co) :
                                bef_co = gpf_before.strokes[j].points[k].co[l]
                                aft_co = gpf_after.strokes[j].points[k].co[l]

                                point.co[l] = ((aft_co - bef_co) * (self.factor/100)) + bef_co
                
                if interpolate == False and clip_switch :
                    gpf_to_copy = None
                    if clip_switch == 'BEFORE':
                        gpf_to_copy = gpf_before
                    if clip_switch == 'AFTER' :
                        gpf_to_copy = gpf_after

                    copy_strokes(gpf_to_copy, new_gpf)

                    #self.layers_to_use.remove(i)
                    #layer.frames.remove(new_gpf)
                    
                    #replace_gpf = layer.frames.copy(gpf_to_copy)
                    #replace_gpf, gpf_before, gpf_after = move_gp_frame(layer, replace_gpf, current_frame)

                    ##replace_gpf.frame_number = current_frame
                    #replace_gpf.keyframe_type ='BREAKDOWN'
                    #
                    #self.layers_to_use.append((layer, gpf_before, replace_gpf, gpf_after, False))
        return {'FINISHED'}

    def draw_callback_px(self, a, b):
        """Draws list of available controls for transformation mode."""
        font_id = 0
        blf.color(font_id, 1.0, 0.36, 0.36, 1.0)
        blf.size(font_id, 15, 72)
        blf.position(font_id, 10, 100, 0)
        blf.draw(font_id, "SMART INBETWEENS MODAL")
        blf.size(font_id, 12, 72)
        blf.position(font_id, 10, 60, 0)
        blf.draw(font_id, "Factor : "+str(self.factor))
        blf.size(font_id, 12, 72)
        blf.position(font_id, 10, 40, 0)
        blf.draw(font_id, 'interpolating '+ str(self.number_objects)+ ' objects '+ str(len(self.fcurves_to_interpolate))+ ' fcurves and ' + str(len(self.layers_to_use))+ ' GP layers ( '+ str(len([i for i in self.layers_to_use if i[4] == True])) + ' sculpted frames ) factor= '+ str(self.factor))
        bpy.context.view_layer.update()

    def modal(self, context, event):
        #print(event.mouse_x)
        if event.type == 'MOUSEMOVE' and self.num_input_bool == False :
            self.factor = event.mouse_x - self.first_mouse_x + 50
            self.interpolate(context)
        elif event.type in {'LEFTMOUSE', 'NUMPAD_ENTER', 'RET'} and event.value == 'PRESS':
            self.random_event.append(event)
            self.execute(context)
            return {'FINISHED'}
        elif event.type in {'RIGHTMOUSE', 'ESC'} and event.value == 'PRESS':
            # on ne recalcule pas self.interpolate(context)
            if len(self.handler) > 0:
                try:
                        bpy.types.SpaceView3D.draw_handler_remove(self.handler[0], 'WINDOW')
                        try:
                            self.handler.clear()
                        except:
                            self.abort(context)
                            return {'CANCELLED'}
                except:
                    self.abort(context)
                    return {'CANCELLED'}
            self.abort(context)
            return {'CANCELLED'}

        if event.type in {'NUMPAD_PLUS'} and event.value == 'PRESS':
            if self.factor < 0 :
                self.factor *= -1  

            self.interpolate(context)
        if event.type in {'NUMPAD_MINUS'} and event.value == 'PRESS':
            if self.factor > 0 :
                self.factor *= -1

            self.interpolate(context)

        elif event.type in {'NUMPAD_0','NUMPAD_1','NUMPAD_2','NUMPAD_3','NUMPAD_4','NUMPAD_5','NUMPAD_6','NUMPAD_7','NUMPAD_8','NUMPAD_9'} and event.value == 'PRESS': 
            if self.num_input_bool == False :
                self.factor = 0
                self.num_input_bool = True
            
            self.factor =  int(str(self.factor) + str(event.type)[-1:])
            self.interpolate(context)

        elif event.type == 'BACK_SPACE' and event.value == 'PRESS' :
            string = str(self.factor)
            print(len(string))
            if len(string) > 0:
                self.factor = 0
                self.interpolate(context)
            else:
                result = int(str(self.factor)[:-1])
                self.factor = result
                self.interpolate(context)
        return {'RUNNING_MODAL'}

    def execute(self, context):
        print("entrée execute")
        #self.initialize(context, self.random_event[0])
        self.interpolate(context)
        if len(self.handler) > 0:
            bpy.types.SpaceView3D.draw_handler_remove(self.handler[0], 'WINDOW')
            self.handler.clear()

        
        return {'FINISHED'}

    def initialize(self, context, event):
        #get initial mouse position
        print('initialize')
        self.first_mouse_x = event.mouse_x
        current_frame = context.scene.frame_current
        self.previous_event_type = ''
        self.above_half = True

        preferences = context.preferences
        addon_prefs = preferences.addons[__name__].preferences

        #for an object, determine keyframes before and after, create inbetween_kf
        
        self.fcurves_to_interpolate.clear()
        self.layers_to_use.clear()

        objects_to_process = []
        if addon_prefs.apply_to == 'OBJECT' or event.alt:
            objects_to_process.append(context.object)
        elif addon_prefs.apply_to == 'OBJECT_CHILDREN' :
            objects_to_process = get_objects_and_childs([context.object], context.scene.objects)
        elif addon_prefs.apply_to == 'COLLECTION' :
            for obj in context.collection.all_objects :
                objects_to_process.append(obj)

        #self.clip_array_index = -1 #it already is
        if addon_prefs.switch_depth_location :
            if addon_prefs.depth_axis == 'X' :
                self.clip_array_index = 0
            if addon_prefs.depth_axis == 'Y' :
                self.clip_array_index = 1
            if addon_prefs.depth_axis == 'Z' :
                self.clip_array_index = 2 
        
        self.number_objects = len(objects_to_process)

        for ob in objects_to_process :
            
            if ob and ob.animation_data and ob.animation_data.action :
                for fcurve in ob.animation_data.action.fcurves :
                    if not fcurve.lock : 
                        kf_before, kf_after = get_before_after_kf(current_frame, fcurve)
                        if kf_before and kf_after and kf_before.interpolation == 'CONSTANT':
                            if fc_is_from_array(fcurve) :
                                ob.keyframe_insert(data_path = fcurve.data_path, index = fcurve.array_index)
                            else : 
                                ob.keyframe_insert(data_path = fcurve.data_path, index = -1)
                            kf_new = get_now_kf(current_frame, fcurve)
                            kf_new.type = 'BREAKDOWN'
                            kf_new.interpolation = 'CONSTANT'
                            kf_new.select_control_point = True

                            if self.clip_array_index != -1 and fcurve.data_path == 'location' and fcurve.array_index == self.clip_array_index :
                                kf_new.co.y = kf_after.co.y
                            
                            self.fcurves_to_interpolate.append((fcurve, kf_before.co.y, kf_new, kf_after.co.y))

            if ob.type == 'GPENCIL' and addon_prefs.create_gp_frame :
                for layer in ob.data.layers :
                    if not layer.lock :
                        gpf_before, gpf_after = get_before_after_gpf(current_frame, layer)
                        if gpf_before and gpf_after :
                            new_gpf = layer.frames.new(current_frame)
                            copy_strokes(gpf_after, new_gpf)
                            #new_gpf = layer.frames.copy(gpf_after)
                            #new_gpf, gpf_before, gpf_after = move_gp_frame(layer, new_gpf, current_frame)
                            #new_gpf.frame_number = current_frame
                            new_gpf.keyframe_type = 'BREAKDOWN'
                            new_gpf.select = True

                            interpolate = False
                            if addon_prefs.interpolate_sculpted :
                                interpolate = sculpted_frames(gpf_before, gpf_after)
                            self.layers_to_use.append((layer, gpf_before, new_gpf, gpf_after, interpolate))
        print('end initialize', len(self.layers_to_use), 'layers and ', len(self.fcurves_to_interpolate), 'fcurves')
    def invoke(self, context, event): 
        print('INVOKE...!!', event.alt)
        self.num_input_bool = False
        self.initialize(context, event)

        if len(self.fcurves_to_interpolate) > 0 or len(self.layers_to_use) > 0:
            context.window_manager.modal_handler_add(self)
            self.handler.clear()
            handler = bpy.types.SpaceView3D.draw_handler_add(self.draw_callback_px, (None, None), 'WINDOW', 'POST_PIXEL')
            self.handler.append(handler)
            print('entered in modal')
            return {'RUNNING_MODAL'}
        else :
            self.abort(context)
            return {'CANCELLED'}
        
class GP_TWEEN_AddonPref(bpy.types.AddonPreferences):
    bl_idname = __name__
    apply_to : bpy.props.EnumProperty(items = [('OBJECT', 'Object', 'Active object only', 0), ('OBJECT_CHILDREN', 'Object children', 'Active object and its children', 1), ('COLLECTION', 'Collection', 'Active collection objects', 2)], default = 'OBJECT_CHILDREN')
    create_gp_frame : bpy.props.BoolProperty(default = True)
    switch_depth_location : bpy.props.BoolProperty(default = True)
    depth_axis : bpy.props.EnumProperty(items = [('X', 'X', 'X', 0), ('Y', 'Y', 'Y', 1), ('Z', 'Z', 'Z', 2)], default = 'Y')

    interpolate_sculpted : bpy.props.BoolProperty(default = True)

    def draw(self, context):
        layout = self.layout
        kc = bpy.context.window_manager.keyconfigs.addon
        col = layout.column()
        for km, kmi in addon_keymaps:
            km = km.active()
            col.context_pointer_set("keymap", km)
            rna_keymap_ui.draw_kmi([], kc, km, kmi, col, 0)

        row = layout.row()
        row.prop( self, 'apply_to', text = 'Apply to ')
        box = layout.box()
        row = box.row()
        row.prop( self, 'create_gp_frame', text = 'create Grease Pencil inbetween' )
        if self.create_gp_frame :
            row = box.row()
            row.separator_spacer()
            row.prop( self, 'interpolate_sculpted', text = 'detect and interpolate between sculpted frames' )

        box = layout.box()
        row = box.row()
        row.prop( self, 'switch_depth_location', text = 'clip location on depth axis' )
        if self.switch_depth_location :
            row = box.row()
            row.separator_spacer()
            row.prop(self, 'depth_axis')

addon_keymaps = []           
default_keymaps = [
    {"name":"gptween.modal", "default_km":"F8", 'alt' : False},
    {"name":"gptween.modal", "default_km":"F8", 'alt' : True}
    ]


classes = [GPTWEEN_OT_MODAL,
    GP_TWEEN_AddonPref,
    ]               
                        
def register():
    for cls in classes :
        bpy.utils.register_class(cls)   

    # Keymapping
    for default_keymap in default_keymaps:
        kc = bpy.context.window_manager.keyconfigs.addon
        if kc :
            km = kc.keymaps.new(name="Dopesheet", space_type='DOPESHEET_EDITOR')
            kmi = km.keymap_items.new(default_keymap['name'], type = default_keymap['default_km'], value = 'PRESS', alt = default_keymap['alt'])
            kmi.active = True
            addon_keymaps.append((km, kmi))

            km = kc.keymaps.new(name="3D View", space_type='VIEW_3D')
            kmi = km.keymap_items.new(default_keymap['name'], type = default_keymap['default_km'], value = 'PRESS', alt = default_keymap['alt'])
            kmi.active = True
            addon_keymaps.append((km, kmi))
def unregister():
    for cls in reversed(classes) :
        bpy.utils.unregister_class(cls)

    # removing keymaps:
    for km, kmi in addon_keymaps:
        km.keymap_items.remove(kmi)
    addon_keymaps.clear()
    
if __name__ == "__main__":
    register()                        