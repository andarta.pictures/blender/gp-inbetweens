# Custom modal for interactions

Modifications on the modal operator for enhanced UX.

# [Download latest as zip](https://gitlab.com/andarta.pictures/blender/gp-inbetweens/-/archive/main/gp-inbetweens-main.zip)


# Installation

Download the repository as a .zip file, access it with blender through Edit/Preferences/Add-ons/Install, then activate it by checking the box in the add-on list.


# User guide

Call the operator with the timeline cursor where you wan't to create an inbetween and hit the keyboard shortcut F8
Moving your mouse will enable on the go modification on the value, but you can also manually enter the value by typing numbers on your keyboard.
3 different modes in the user preferences allows you to generate inbetweens over different sets of objects : (selectd / obj childrens/collection) 
Alt F8 forces to slected object only mode

# Warning

This tool is in development stage, thank you for giving it a try ! please report any bug or suggestion to t.viguier@andarta-pictures.com

# Visit us 

[https://www.andarta-pictures.com/](https://www.andarta-pictures.com/)